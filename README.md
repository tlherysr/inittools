## Init Tools
This repo contains my initial linux installation tools and basic configurations to make the process easy.

## Tools
- JSParse
- knockpy
- sqlmap-dev
- wpscan
- nmap
- unfurl
- waybackurls
- httprobe
- Seclists collection
- ffuf
- Sublist3r
- Aquatone
- webscreenshot

The script also grabs the configuration from the zshrc file in here.  

## Installing  
git clone https://gitlab.com/tlherysr/inittools.git  
cd inittools  
chmod +x install.sh  
./install.sh  
